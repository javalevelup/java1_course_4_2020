package org.levelup.app;

import org.levelup.app.config.AppConfiguration;
import org.levelup.app.model.Student;
import org.levelup.app.service.StudentService;
import org.levelup.app.service.file.FileStudentService;

import java.util.Collection;

public class App {

    public static void main(String[] args) {
        AppConfiguration instance = AppConfiguration.getInstance();
        System.out.println(instance.getStudentFilepath());
        System.out.println(instance.getFacultyFilepath());

        StudentService studentService = new FileStudentService();
        studentService.saveStudent(new Student("Егор","Егоров", 2, 5410));

        long startWithLoadingFromFile = System.nanoTime(); // <- получение текущего времени в наносекундах
        Collection<Student> students = studentService.getStudents();
        long endWithLoadingFromFile = System.nanoTime();

        System.out.println(students);

        long startWithCache = System.nanoTime();
        students = studentService.getStudents();
        long endWithCache = System.nanoTime();
        System.out.println(students);

        long withFile = endWithLoadingFromFile - startWithLoadingFromFile;
        long withCache = endWithCache - startWithCache;
        System.out.println("Время с загрузкой из файла: " + withFile + "ns, " + (withFile / 1000000d) + "ms");
        System.out.println("Время без загрузки из файла: " + withCache + "ns, " + (withCache / 1000000d) + "ms");
    }

}
