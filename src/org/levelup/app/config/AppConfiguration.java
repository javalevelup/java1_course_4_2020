package org.levelup.app.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AppConfiguration {

    private static AppConfiguration instance = new AppConfiguration();

    // Расположение файла со студентами
    private String studentFilepath;
    // Расположение файла с факультетами
    private String facultyFilepath;

    // Singleton <- позволяет сделать так, что объект этого класса является единственным в приложении
    private AppConfiguration() {}

    // AppConfiguration conf = AppConfiguration.getInstance();
    // AppConfiguration conf2 = AppConfiguration.getInstance();
    // -> conf == conf2 -> true
    public static AppConfiguration getInstance() {
        return instance;
    }

    static {
        readConfigurationFile();
    }

    private static void readConfigurationFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader("resources/application.properties"))) {
            String line;
            while ( (line = reader.readLine()) != null ) {
                // String#trim() <- убирает пробелы до первого символа и после последнего
                // "    2344  ".trim() -> "2344"
                if (!line.trim().isEmpty() && !line.startsWith("#")) {
                    String[] sides = line.split("="); // "some.key=value".split("=") -> ["some.key", "value"]
                    if (sides[0].equals("student.file.path")) {
                        instance.studentFilepath = sides[1];
                    } else if (sides[0].equals("faculty.file.path")) {
                        instance.facultyFilepath = sides[1];
                    }
                }
            }

        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
    }

    public String getStudentFilepath() {
        return studentFilepath;
    }

    public String getFacultyFilepath() {
        return facultyFilepath;
    }

    //     Блоки инициализации
//     Обычные и статический
//
//    {
//
//    }
//
//    static {
//
//    }

}
