package org.levelup.app.model;

import org.levelup.app.service.StudentService;

import java.util.Collection;

/**
 * 1. Может ли enum наследоваться от другого класса? Нет
 * 2. Может ли enum наследоваться от другого enum? Нет
 * 3. Может ли enum наследоваться от абстрактного класса? Нет
 * 4. Может ли enum реализовывать интерфейс? Да
 * 5. Может ли enum иметь абстракнтые методы? Да
 */
// extends Enum
public enum CourseWorkStatus implements StudentService {
    // public static final CourseWorkStatus APPROVED = new CourseWorkStatus();
    APPROVED(10){
        @Override
        public Collection<Student> getStudents() {
            return null;
        }

        @Override
        public int getPointFactor() {
            return 1;
        }
    },
    DECLINED() {
        @Override
        public Collection<Student> getStudents() {
            return null;
        }

        @Override
        public int getPointFactor() {
            return 2;
        }
    };

    private int numberOfPoints;

    CourseWorkStatus(int numberOfPoints) {
        this.numberOfPoints = numberOfPoints;
    }

    CourseWorkStatus() {
        this.numberOfPoints = 1;
    }

    public int getNumberOfPoints() {
        return numberOfPoints;
    }

    public abstract int getPointFactor();

    @Override
    public void saveStudent(Student student) {

    }
}
