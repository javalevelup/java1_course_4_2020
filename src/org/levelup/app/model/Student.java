package org.levelup.app.model;

public class Student {

    private String name;
    private String lastName;

    private int courseNumber;
    private int studentIdNumber;

    public Student(String name, String lastName, int courseNumber, int studentIdNumber) {
        this.name = name;
        this.lastName = lastName;
        this.courseNumber = courseNumber;
        this.studentIdNumber = studentIdNumber;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getCourseNumber() {
        return courseNumber;
    }

    public int getStudentIdNumber() {
        return studentIdNumber;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", courseNumber=" + courseNumber +
                ", studentIdNumber=" + studentIdNumber +
                '}';
    }
}
