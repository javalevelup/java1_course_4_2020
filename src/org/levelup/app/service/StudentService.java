package org.levelup.app.service;

import org.levelup.app.model.Student;

import java.util.Collection;

public interface StudentService {

    Collection<Student> getStudents();

    void saveStudent(Student student);

}
