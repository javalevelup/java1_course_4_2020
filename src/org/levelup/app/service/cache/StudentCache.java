package org.levelup.app.service.cache;

import org.levelup.app.config.AppConfiguration;
import org.levelup.app.model.Student;
import org.levelup.app.service.file.FileService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

// Класс, который будет хранить студентов в оперативной памяти (считывая их из файла)
public class StudentCache {

    private final FileService fileService;

    private Collection<Student> cache; // <- именно здесь хранятся студенты из файла
    private boolean needUpdate; // <- переменная, которая показывает нам, что нужно (или не нужно) обновить кэш

    public StudentCache() {
        this.fileService = new FileService();

        this.cache = new ArrayList<>();
        this.needUpdate = true;
    }

    public Collection<Student> getStudents() {
        // if (isValid == false) <- плохой стиль
        if (needUpdate) {
            System.out.println("Обновляем данные в кэше...");
            // обновить кэш (считываем данные из файла)
            updateCache();
            needUpdate = false;
        }
        return cache;
    }

    // Этот метод будет говорить, что наш кэш невалиден (неправильный) и его обновить
    public void invalidateCache() {
        needUpdate = true;
    }

    private void updateCache() {
        cache = fileService.readLines(AppConfiguration.getInstance().getStudentFilepath()) // Collection<String>
                .stream()
                // .map(line -> parseStudent(line))
                // method reference (ссылка на метод)
                // lambda: Function<A,R># R apply(A a)
                //          Student parseStudent(String)
                //          аргумент lambda имеет тип String
                //          R в Function и у метода parseStudent есть возвращаемое значение (не void)
                //  то, мы можем заменить вызов метода parseStudent на "ссылку на метод"
                .map(this::parseStudent)
                // в левой части - объект, у которого вызывается метод
                // в правой части - имя метода, который мы вызываем
                .collect(Collectors.toList());
    }

    private Student parseStudent(String line) {
        String[] values = line.split(",");
        return new Student(
                values[0],
                values[1],
                Integer.parseInt(values[2]),
                Integer.parseInt(values[3])
        );
    }

}
