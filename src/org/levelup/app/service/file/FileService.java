package org.levelup.app.service.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class FileService {

    public Collection<String> readLines(String filepath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filepath))) {
            Collection<String> lines = new ArrayList<>();
            String line;

            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            return lines;

        } catch (IOException exc) {
            System.out.println("Ошибка при загрузке студентов из файла: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

    public void writeLine(String filepath, String line) {
        try (FileWriter writer = new FileWriter(filepath, true)) {
            writer.write("\n");
            writer.write(line);
        } catch (IOException exc) {
            System.out.println("Ошибка при записи студента в файл: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

}
