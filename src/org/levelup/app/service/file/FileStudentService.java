package org.levelup.app.service.file;

import org.levelup.app.config.AppConfiguration;
import org.levelup.app.model.Student;
import org.levelup.app.service.StudentService;
import org.levelup.app.service.cache.StudentCache;

import java.util.Collection;

public class FileStudentService implements StudentService {

    // final - означает константу
    private final StudentCache cache;
    private final FileService fileService;

    public FileStudentService() {
        this.cache = new StudentCache();
        this.fileService = new FileService();
    }

    @Override
    public Collection<Student> getStudents() {
        return cache.getStudents();
    }

    @Override
    public void saveStudent(Student student) {
        final String line = String.join(",",
                student.getName(),
                student.getLastName(),
                String.valueOf(student.getCourseNumber()),
                String.valueOf(student.getStudentIdNumber())
        );   // > name,lastName,courseNumber,studentIdNumber

        fileService.writeLine(AppConfiguration.getInstance().getStudentFilepath(), line);

        cache.invalidateCache();
    }

}
