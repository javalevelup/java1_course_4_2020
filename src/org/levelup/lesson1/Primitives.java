package org.levelup.lesson1;

// Полное имя - org.levelup.lesson1.Primitives
public class Primitives {

    public static void main(String[] args) {
        // Line comment
        /*
            Comment block
        */

        int first;  // объявление переменной
        first = 9584; // запись значения в переменную first
        System.out.println(first); // вывод значения переменной first на экран

        int second = 4352;  // инициализация переменной

        // Конкатенация строк
        // "1 строка" + "2 строка" = "1 строка2 строка"
        // "Значение второй переменной: " + 4352 -> "Значение второй переменной: " + "4352" -> "Значение второй переменной: 4352"
        System.out.println("Значение второй переменной: " + second);

        int result = first * second;
        System.out.println("Прозведение двух чисел: " + result);

        char spaceChar = ' ';

        double doubleValue = 5643.99999999;
        System.out.println(doubleValue);

        int r = 10 % 3;  // вычисление остатка от деления 10 на 3
        System.out.println("Остаток: " + r);


        int inc = 10;
        // ++inc - префиксный инкремент -> inc = inc + 1;
        // inc = 11
        // int++ - постфиксный инкремент
        // inc = 12

        System.out.println(inc++); // 10
        System.out.println(++inc); // 12

        // System.out.println(inc++);
        //      1. System.out.println(inc); -> 10
        //      2. inc = inc + 1; -> inc = 11

        // System.out.println(++inc);
        //      1. inc = inc + 1; -> inc = 12
        //      2. System.out.println(inc); -> 12


        int fValue = 40;
        int sValue = 20;

        int fResult = fValue++ + ++sValue;
        // 1. ++sValue -> 21
        // 2. fValue + sValue -> 40 + 21 = 61
        // 3. fValue++ -> 41
        int sResult = ++fValue + sValue++;
        // 1. ++fValue -> 42
        // 2. fValue + sValue -> 42 + 21 = 63
        // 3. sValue++ -> 22

        // fValue
        // sValue
        System.out.println("fValue: " + fValue); // 42
        System.out.println("sValue: " + sValue); // 22
        System.out.println("fResult: " + fResult); // 61
        System.out.println("sResult: " + sResult); // 63

        // Преобразование типов (приведение типов)
        int intValue = 54;
        long longValue = intValue; // неявное преобразование (расширяющее преобразование)

        byte byteValue = (byte) longValue; // явное преобразование (сужающее преобразование)

        int intFromDouble = (int) doubleValue;
        System.out.println(intFromDouble);


        long res = (long) (doubleValue + longValue);

    }

}
