package org.levelup.lesson10;

import org.levelup.app.model.CourseWorkStatus;
import org.levelup.app.model.Student;

import java.util.ArrayList;
import java.util.Collection;

public class EnumExample {

    public static void main(String[] args) {
        // filteredByStatus("");
        // filteredByStatus("APPROVED2");
        // filteredByStatus("declined");

        CourseWorkStatus status = CourseWorkStatus.DECLINED;
        System.out.println("Number of points: " + status.getNumberOfPoints());

        filteredByStatus(status);
        filteredByStatus(CourseWorkStatus.APPROVED);

        String name = status.name();
        System.out.println(name);

        CourseWorkStatus fromString = CourseWorkStatus.valueOf("APPROVED");
        int ordinal = fromString.ordinal();
        System.out.println(ordinal);

        CourseWorkStatus[] values = CourseWorkStatus.values();
        System.out.println(values[0].name());
        System.out.println(values[1].name());
    }

    static Collection<Student> filteredByStatus(String status) {
        if (status.equals("APPROVED")) {
            return new ArrayList<>();
        } else if (status.equals("DECLINED")) {
            return new ArrayList<>();
        }
        throw new RuntimeException("No such status");
    }

    static Collection<Student> filteredByStatus(CourseWorkStatus status) {
        if (status == CourseWorkStatus.APPROVED) {
            return new ArrayList<>();
        } else if (status == CourseWorkStatus.DECLINED) {
            return new ArrayList<>();
        }
        throw new RuntimeException("No such status");
    }


}
