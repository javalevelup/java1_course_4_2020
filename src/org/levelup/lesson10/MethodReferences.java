package org.levelup.lesson10;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MethodReferences {

    public static void main(String[] args) {
        Collection<Integer> ints = new ArrayList<>();
        ints.add(4);
        ints.add(5);
        ints.add(6);
        ints.add(7);
        // forEach(Consumer<> consumer)
        // Consumer<T># void accept(T t)
        ints.forEach(value -> System.out.println(value));
        PrintStream ps = System.out;
        ints.forEach(ps::println);
        ints.forEach(System.out::print);
        List<String> numbers = ints.stream()
                //
                // .map(el -> el.toString()) -> может привести к NPE
                // .map(Object::toString)

                // String.valueOf(obj)
                .map(String::valueOf) // .map(el -> return String.valueOf(el))
                .collect(Collectors.toList());
        List<Integer> increased = ints.stream()
                // .map(el -> increment(el))
                // .map(this::increment)
                .map(MethodReferences::increment)
                .collect(Collectors.toList());

        StringBuilder prefix = new StringBuilder("USER1:");
        // StringBuilder#append(String s)
        // USER1:
        // sb.append("4") -> USER1:4
        List<String> list = numbers.stream()
                // .map(el -> new StringBuilder(el))
                // "4"
                // "5"
                // "6"
                // "7"
                //
                // "USER1:4"
                // "USER1:5"
                // "USER1:6"
                // "USER1:7"
                // .map(el -> prefix + el)
                // .map(el -> prefix.append(el))
                .filter(Objects::nonNull)
                .map(el -> {
                    try {
                        return prefix.append(el);
                    } catch (Exception exC) {
                        return new StringBuilder("1");
                    }
                })
                // .map(sb -> sb.toString())
                .map(StringBuilder::toString)
                // .map(String::valueOf)
                .collect(Collectors.toList());
    }

    static Integer increment(Integer el) {
        return ++el;
    }

}
