package org.levelup.lesson2;

import java.util.Random;

public class Array {

    public static void main(String[] args) {

        // У каждого массива есть длина
        double[] prices = new double[5];
        // Записать значение в ячейку массива
        prices[0] = 545.34;
        prices[1] = 67.43;
        prices[3] = 956.23;

        // Чтение значения из массива (по индексу)
        System.out.println("Значение в 0 индексе: " + prices[0]);

        // System.out.println(prices); D[@4955u38
        int arrayLength = prices.length; // length = 5
        for (int i = 0; i < arrayLength; i++) {
            System.out.println("prices[" + i + "] = " + prices[i]);
        }

        int[] generatedNumbers = new int[10];
        Random r = new Random();

        for (int i = 0; i < generatedNumbers.length; i++){
            generatedNumbers[i] = r.nextInt(32);
        }

        for (int i = 0; i < generatedNumbers.length; i++) {
            System.out.println("generatedNumbers[" + i + "] = " + generatedNumbers[i]);
        }

        int[] array = { 7, 645, 234, 54, 4, 8, 98 };
        // array[0] = 5;
        // array[1] = 0;
        // array[2] = 55;
        for (int i = 0; i < array.length; i++) {
            System.out.println("array[" + i + "] = " + array[i]);
        }

    }

}
