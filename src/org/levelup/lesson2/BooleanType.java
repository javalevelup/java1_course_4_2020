package org.levelup.lesson2;

public class BooleanType {

    // psvm
    public static void main(String[] args) {

        int var = 545;
        boolean result = var % 5 == 0; // 1. var % 5 -> 0; 2. 0 == 0 -> true
        // sout
        System.out.println(result);

        System.out.println(var % 5 == 0);

    }

}
