package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

@SuppressWarnings("ALL")
public class GuessNumber {

    public static void main(String[] args) {

        // Генерация псевдо-случайных чисел
        Random r = new Random();
        // nextInt() -> nextInt(32) -> [0, 32)
        // [0, 3], [0, 4) -> nextInt(4)
        // [4, 9], [4, 10) -> nextInt(6) [0, 6): nextInt(6) + 4: [4, 10)
        int secretNumber = r.nextInt(4); // Число, которое мы должны угадать

        // Для того, чтобы считать с клавиатуры
        // sc - переменная (как secretNumber)
        // Scanner - тип переменной (как int)
        Scanner sc = new Scanner(System.in);
        // sc.nextInt() - вызов метода nextInt(). Этот метод находится в классе Scanner
        System.out.println("Введите число:");
        int enteredNumber = sc.nextInt(); // Число, которое вводит пользователь

// Вывести информацию, угадал ли пользователь число или нет
//        if (secretNumber == enteredNumber) {
//            System.out.println("Вы ввели правильное число!");
//        } else {
//            System.out.println("Вы ввели неправильное число!");
//        }

// 1 вариант множественного if..else
//        if (secretNumber == enteredNumber) {
//            System.out.println("Вы ввели правильное число!");
//        } else {
//            System.out.println("Вы ввели неправильное число!");
//            if (secretNumber > enteredNumber) {
//                System.out.println("Вы ввели число, которое меньше, чем загаданное");
//            } else {
//                System.out.println("Вы ввели число, которое больше, чем загаданное");
//            }
//        }

        // 2 вариант: воспользоваться конструкцией if..else if..
        if (secretNumber == enteredNumber) {
            // int localVariable = 39;
            System.out.println("Вы угадали!");
        } else if (secretNumber > enteredNumber) {
            // System.out.println(localVariable); <- переменная не видна
            System.out.println("Вы ввели число, которое меньше, чем загаданное!");
        } else {
            System.out.println("Вы ввели число, которое больше, чем загаданное!");
        }
        // System.out.println(localVariable); <- переменная не видна
//        if (secretNumber == enteredNumber) {
//            System.out.println("Вы угадали!");
//        }
//        if (secretNumber > enteredNumber) {
//            System.out.println("Вы ввели число, которое меньше, чем загаданное!");
//        }
//        if (secretNumber < enteredNumber) {
//            System.out.println("Вы ввели число, которое больше, чем загаданное!");
//        }

    }

}
