package org.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {

        for (int counter = 0; counter < 10; counter++) {
            System.out.println("I like Java!");
        }

        int i = 0;
        for (; i < 10; i = i + 2) {
            System.out.println(i);
        }

        // for (;;) {}

        for (;;) {
            System.out.println("Итерация бесконечного цикла");
            i = i + 10;
            if (i > 100) {
                // Прерывание цикла
                break;
            }
        }

        while (i < 1000) {
            System.out.println(i);
            i = i + 50;
        }

    }

}
