package org.levelup.lesson3;

// Subclass is as Superclass

// Object <- Point <- TriplePoint <- D4Point
public class D4Point extends TriplePoint {

    private int t;

    public D4Point(int z, int t) {
        // super();
        super(z);
        this.t = t;
        System.out.println("D4Point constructor");
    }

    public void changeT(int t) {
        if (t > 0) {
            this.t = t;
        }
    }

    // setter
    // public void set<Название поля>(<тип поля> имяПоля) {}
    public void setT(int t) {
        this.t = t;
    }
    // getter
    // public <тип поля> get<Название поля>() {}
    public int getT() {
        return t;
    }

}
