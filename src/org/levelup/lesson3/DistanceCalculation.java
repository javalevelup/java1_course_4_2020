package org.levelup.lesson3;

public class DistanceCalculation {

    public static void main(String[] args) {
        Point a = new Point(4, 3, "A");
        Point b = new Point(8, 6, "B");

        PointService pointService = new PointService();
        double result = pointService.calculateDistance(a, b);

        System.out.println("Расстояние (через PointService): " + result);

        double res = a.calculateDistance(b);
        System.out.println("Расстояние (через Point): " + res);
    }

}
