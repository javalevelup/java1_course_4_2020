package org.levelup.lesson3;

import java.util.Objects;

// Модификатор доступа
//  private - поле/метод/конструктор доступны только внутри класса
//  default-package (private-package) - поле/метод/конструктор доступны внутри пакета
//  protected - поле/метод/конструктор доступны внутри пакета или внутри классов-наследников, даже если они находятся в другом пакета
//  public - поле/метод/конструктор доступны везде
public class Point {

    // this - ссылка на объект (ссылка на самого себя)

    // Переменные класса, поля класса, атрибуты класса, field
    private int x;
    public int y;
    public String name; // Название точки

    // Point p;

    // Конструктор
    // <модификатор доступа> названиеКонструктора(<тип аргумента 2> названиеАргумента1, <тип аргумента 2> названиеАргумента2)
    // названиеКонструктора - имя класса
    public Point() {
//        x = 0;
//        y = 0;
//        name = "(no name)";
        this(0, 0, "(no name)");
        System.out.println("Point constructor");
    }

    public Point(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
        // this.z = z;
    }

    // private Point() {}

    // Метод
    // <модификатор доступа> <тип возвращаемого значения> названиеМетода(<тип аргумента 2> названиеАргумента1, <тип аргумента 2> названиеАргумента2) {}
    // тип возвращаемого значения: void - это означает, что метод ничего не возвращает
    public void changeX(int newX) {
        // тело метода
        if (newX != x) {
            x = newX;
        }
    }

    // Метод может не иметь аргументов
    public int getX() {
        return x; // Результатом работы метода будет значение поля х
    }

    public int getY() {
        return y;
    }

    // Сигнатура метода - имя + типы аргументов (причем важен как порядок, так и типы)
    // Изменения точки
    // 1 метод - изменить значения x и y
    // point.changePoint(4, 3);
    public void changePoint(int newX, int newY) {
        x = newX;
        y = newY;
        // changePoint(newX, newY, name);
    }

    // point.changePoint(4, 3);
    // public void changePoint(int newY, int newX) {
    //        x = newX;
    //        y = newY;
    //        // changePoint(newX, newY, name);
    //    }

    // Перегрузка метода (overloading) - когда два метода имеют одинаковое название
    // 2 метод - изменить значения x, y и name
    public void changePoint(int newX, int newY, String newName) {
        changePoint(newX, newY);
        // x = newX;
        // y = newY;
        name = newName;
    }

    // m + int + double
    // class A { public int m(int a, double b); }
    // 1. void m(int a, double b); - m + int + double - нет
    // 2. int m(int b, double a); - m + int + double - нет
    // 3. int m(double a, int b); - m + double + int - да
    // 4. int m(double a, double b); - m + double + double - да
    // 5. int m(int a, int b); - m + int + int - да
    // 6. double m(int a, float b); - m + int + float - да

    public double calculateDistance(Point other) {
        double k1 = Math.abs(y - other.y);
        double k2 = Math.abs(this.x - other.x);
        // double k2 = Math.abs(x - other.x);

        return Math.sqrt(k1 * k1 + k2 * k2);
    }

    // p1.equals(null) -> false
    // p1 == null -> false
    // null == null -> true

    // p1.equals("some string");
    // p1.equals(new Object());
    @Override
    public boolean equals(Object object) {
        // x, y, name
        // this - тот объект, который вызвал метод equals
        // object - тот объект, который был передан в качестве параметра
        if (this == object) return true;

        // 2 способа проверить класс
        // 1 способ: простой и не особо правильный (логически)

        // instanceof
        // <object> instanceof <Class> -> true/false
        // null instanceof <Any class> -> false

        // class TriplePoint extends Point
        // если object является объектом TriplePoint, то тогда object instanceof Point вернут true
        // if (!(object instanceof Point)) return false;

        // 2 способ: сравнивать объекты класса Class
        if (object == null || getClass() != object.getClass()) return false;
        // if (object == null) {
        //     return false;
        // }
        Point other = (Point) object; // -> ClassCastException
        return x == other.x &&
                y == other.y &&
                (name != null && name.equals(other.name));
                // Objects.equals(name, other.name);
                // name.equals(other.name);
    }

    @Override
    public int hashCode() {
//        int result = 17;
//
//        result += result * 31 + x;
//        result += result * 31 + y;
//        result += result * 31 + name.hashCode();
//
//        return result;
        return Objects.hash(x, y, name);
    }

}
