package org.levelup.lesson3;

public class PointApp {

    public static void main(String[] args) {

        // Локальные переменные - переменные, которые объявлены внутри метода
        int x = 545;
        int y = 675;

        // Объект, ссылка, экземпляр, object, reference, instance
        Point leftPoint = new Point(); // 3954a
        // Найти память
        // Зарезервировать память
        // Сгенерировать ссылку
        // Вернуть ссылку (записать ссылку в объект/переменную)
        leftPoint.changeX(10); // leftPoint.x = 10;
        leftPoint.y = 42;

        System.out.println(leftPoint.getX() + " " + leftPoint.y);

        Point rightPoint = new Point(); // 5211d
        rightPoint.changeX(12);
        rightPoint.y = 53;

        System.out.println(rightPoint.getX() + " " + rightPoint.y);

        leftPoint.name = "A"; // new String("A")

        leftPoint.changePoint(4, 66);
        rightPoint.changePoint(5, 12, "B");

        Point emptyPoint = new Point();
        System.out.println(emptyPoint.getX() + " " + emptyPoint.y + " " + emptyPoint.name);

        Point pointC = new Point(34, 23, "C");

        System.out.println(pointC.getX() + " " + pointC.y + " " + pointC.name);

        System.out.println();

        TriplePoint tp = new TriplePoint(21);
        tp.changeX(405);
        tp.setZ(43);
        System.out.println("TP: " +  tp.getX() + " " + tp.getZ());

        System.out.println();
        D4Point d4 = new D4Point(14, 15);
        d4.setZ(384);

//        if (t > 0) {
//            d4.setT(t); // d4.t = t;
//        }
        d4.setT(234);
        d4.changeX(23);


    }

}
