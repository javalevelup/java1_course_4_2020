package org.levelup.lesson3;

// Service - класс, который содержит методы для работы с какими-либо сущностям
public class PointService {

    public double calculateDistance(Point p1, Point p2) {
        double k1 = Math.abs(p1.getY() - p2.getY());
        double k2 = Math.abs(p1.getX() - p2.getX());

//        double res = Math.sqrt(k1 * k1 + k2 * k2);
//        return res;
        return Math.sqrt(k1 * k1 + k2 * k2);
    }

}
