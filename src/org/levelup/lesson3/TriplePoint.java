package org.levelup.lesson3;

// DRY - don't repeat yourself

// Класс, от которого унаследовались - superclass, base class, родитель, предок
// Класс, который наследует друго класс - subclass, подкласс, ребенок
public class TriplePoint extends Point {

//    private int x;
//    int y;
//    public String name;
    private int z;

    public TriplePoint(int z) {
        super();
        this.z = z;
        System.out.println("TriplePoint constructor");
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getZ() {
        return z;
    }

}
