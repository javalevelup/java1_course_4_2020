package org.levelup.lesson4;

import org.levelup.lesson3.Point;

@SuppressWarnings("ALL")
public class PrimitiveVsReferences {

    public static void main(String[] args) {
        // by pass value

        int var = 10;       // 5ab2c
        Point point = new Point(10, 10, "A"); // 987cb

        var = changePrimitive(var);
        changePrimitiveInReference(point); // point.y = 20;

        System.out.println("var: " + var);
        System.out.println("y: " + point.y); // 987cb - point, 987ab - point.y
        point.y = 30;
        System.out.println("y2: " + point.y);
    }

    static int changePrimitive(int variable) { // ab321
        variable = 20; // ab321
        return variable;
    }

    static void changePrimitiveInReference(Point point) { // 987cb - point, 987ab - point.y
        point.y = 20; // 987ab - point.y -> 20
    }

}
