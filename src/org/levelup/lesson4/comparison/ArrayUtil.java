package org.levelup.lesson4.comparison;

import java.util.Objects;

public class ArrayUtil {

    // array - массив, в котором ищем объет obj
    // return int - индекс элемента массива
    // если объекта нет в массиве, то тогда вернем -1
    public int indexOf(Object[] array, Object obj) {
        for (int i = 0; i < array.length; i++) {
            // array[0] - String: будет вызан метод equals из класса String
            // array[1] - Object: будет вызан метод equals из класса Object
            // array[2] - Point: будет вызан метод equals из класса Point
            // array[3] - Shape: будет вызан метод equals из класса Shape
            if (array[i].hashCode() == obj.hashCode() && array[i].equals(obj)) {
                return i;
            }
        }
        return -1;
    }

}
