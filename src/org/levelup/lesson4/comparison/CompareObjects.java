package org.levelup.lesson4.comparison;

import org.levelup.lesson3.Point;
import org.levelup.lesson4.shapes.Rectangle;

@SuppressWarnings("ALL")
public class CompareObjects {

    public static void main(String[] args) {
        Point p1 = new Point(3, 4, "A");    // 5ab32
        Point p2 = new Point(3, 4, "A");    // 980ab
        Point p3 = p1; // 5ab32

        boolean isEquals = p1 == p3;  // сравнение ссылок -> 5ab32 == 980ab -> false
        System.out.println("Результат сравнения: " + isEquals);

        boolean isEq = p1.equals(p2); // boolean equals(Object obj)
        System.out.println("Результат сравнения через equals: " + isEq);

        Object[] array = {
            "Some string",
            54,
            6543.3,
            p1,
            new Rectangle(3, 5)
        };

        ArrayUtil util = new ArrayUtil();
        int indexP2 = util.indexOf(array, p2);
        System.out.println("indexP2: " + indexP2);

        int index65 = util.indexOf(array, 65);
        System.out.println("index65: " + index65);

    }

}
