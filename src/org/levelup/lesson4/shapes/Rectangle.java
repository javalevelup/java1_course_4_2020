package org.levelup.lesson4.shapes;

public class Rectangle extends Shape {

    // sides[] = [w, l, w, l];
    public Rectangle(int width, int length) {
        super(new int[] {width, length, width, length} );
    }

    // calculateRectangleSquare() {}

    // Переопределение методов (overriding) - изменение тела метода в наследниках
    @Override
    public double calculateSquare() {
        // super.calculateSquare(); - вызов метода из родительского класса
        // sides[0] - width
        // sides[1] - length
        return sides[0] * sides[1];
    }

    @Override
    public double calculatePerimeter() {
        return sides[0] * 2 + sides[1] * 2;
    }

    public int getWidth() {
        return sides[0];
    }

    public int getLength() {
        return sides[1];
    }

}
