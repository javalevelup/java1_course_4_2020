package org.levelup.lesson4.shapes;

public class Shape {

    protected int[] sides; // длины сторон фигуры

    public Shape(int[] sides) {
        this.sides = sides;
    }

    public double calculateSquare() {
        return 0.0d;
    }

    public double calculatePerimeter() {
        return 0.0d;
    }

}
