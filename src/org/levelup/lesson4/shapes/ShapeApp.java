package org.levelup.lesson4.shapes;

@SuppressWarnings("ALL")
public class ShapeApp {

    public static void main(String[] args) {
        Shape shape = new Shape(new int[] {3, 4, 5, 8, 6, 3} );
        Rectangle rectangle = new Rectangle(5, 6);
        // Shape rec = new Rectangle(4, 3);

        System.out.println("Площадь Shape: " + shape.calculateSquare());
        System.out.println("Периметр Shape: " + shape.calculatePerimeter());
        System.out.println("Площадь Rectangle: " + rectangle.calculateSquare());
        System.out.println("Периметр Rectangle: " + rectangle.calculatePerimeter());

        // Приведение ссылочных типов
        System.out.println("Ширина прямоугольника: " + rectangle.getWidth());
        Shape fromRectangle = rectangle; // неявное приведение типов
        // System.out.println(fromRectangle.getWidth());
        Object objFromShape = fromRectangle;
        Object objFromRectangle = rectangle;

        // dobule d = 30;
        // int i = (int) d;
        Rectangle recFromShape = (Rectangle) fromRectangle; // явное приведение типов
        // Rectangle rectangleFromShape = (Rectangle) shape;

        Shape[] shapes = new Shape[4];
        shapes[0] = shape;
        shapes[1] = rectangle;  // неявное приведение типов: Shape s = rectangle; shapes[1] = s;
        shapes[2] = new Shape(new int[]{3, 5, 6, 7});
        shapes[3] = new Rectangle(2, 8);

        displaySquares(shapes);

        // displaySquare(rectangle);
    }

    static void displaySquares(Shape[] shapes) {
        System.out.println("Площадь 4ой фигуры: " + shapes[3].calculateSquare());
        for (int i = 0; i < shapes.length; i++) {
            System.out.println("Площадь " + i + "ой фигуры: " + shapes[i].calculateSquare());
        }
    }

    static void displaySquare(Shape shape) {
        System.out.println("Площадь фигуры: " + shape.calculateSquare());
    }

    // static void displaySquares(Rectangle[] recs) {}

    // static void displaySquare(Shape shape) {}

    // static void displaySquare(Rectangle rectangle) {}
    // static void displaySquare(Triangle triangle) {}
    // static void displaySquare(Square square) {}

}
