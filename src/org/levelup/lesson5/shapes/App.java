package org.levelup.lesson5.shapes;

public class App {

    public static void main(String[] args) {

        // Shape shape = new Shape();
        Shape shape = new Triangle();
        shape.calculateSquare();
        shape.calculatePerimeter();

    }

}
