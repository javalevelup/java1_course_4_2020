package org.levelup.lesson5.shapes;

//Особенности абстрактного класса
//  Нельзя создать объект абстрактного класса
//  Может содержать асбтрактные методы (методы без тела)
public abstract class Shape {

    protected int[] sides;

    public abstract double calculateSquare();

    public abstract double calculatePerimeter();

    public int[] getSides() {
        return sides;
    }

    public int getSide(int numberOfSide) {
        return sides[numberOfSide];
    }

}
