package org.levelup.lesson5.shapes;

public class Triangle extends Shape {

    @Override
    public double calculateSquare() {
        return 0;
    }

    @Override
    public double calculatePerimeter() {
        return 0;
    }

    public int getFirstSide() {
        return getSide(0);
    }

}
