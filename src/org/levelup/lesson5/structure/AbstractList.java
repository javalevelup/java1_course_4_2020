package org.levelup.lesson5.structure;

// class AbstractList<T> implements Structure<T> - что AbstractStructure задает тип для Structure
// class AbstractList<T, K> implements Structure<K>
public abstract class AbstractList<T> implements Structure<T> {

    protected int size;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

}
