package org.levelup.lesson5.structure;

public class App {

    public static void main(String[] args) {

        // Structure structure = new DynamicArray(3);
        Structure<Integer> structure = new DynamicArray<>(3);
        structure.add(4); // int (4) -> Integer (4) -> Object
        structure.add(5);
        structure.add(6);
        structure.add(7);
        structure.add(8);
        structure.add(9);
        structure.add(10);
        structure.add(11);
        structure.add(12);

        // compilation error \|
        // structure.add("string");
        // structure.add(new Object());

    }

}
