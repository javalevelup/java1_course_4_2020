package org.levelup.lesson5.structure;

// Список - набор однотипных элементов
// СД - список на основе массива (динамический массив)

// Данные мы храним в массиве
// Добавляем элементы в структуры - данные хранятся в порядке добавления
// Автоматически расширяем массив при полной заполнении массива
public class DynamicArray<T> extends AbstractList<T> {

    // <T>: new T <- запрещено
    // <T>: new T[] <- запрещено
    private Object[] elements;
    // количество добавленных элементов, сколько реальных элементов в массиве
    // private int size;

    public DynamicArray(int initialCapacity) {
        this.elements = new Object[initialCapacity];
    }

    @Override
    public void add(T value) {
        if (size == elements.length) {
            // Нам нужно увеличить размер массива
            // elements -> 53ab12
            // oldElements -> 53ab12
            Object[] oldElements = elements;
            // elements -> 12453a
            // oldElements -> 53ab12
            elements = new Object[(int)(elements.length * 1.5)];
            // Копируем из oldElements в elements все значения
            System.arraycopy(oldElements, 0, elements, 0, oldElements.length);
        }
        elements[size++] = value;
        // size = 0
        //      -> elements[0] = value; size++ -> size = 1
        // size = 1
        //      -> elements[1] = value; size++ -> size = 2
    }

    @Override
    public void removeByValue(int value) {

    }

}
