package org.levelup.lesson5.structure;

// Класс, который представляет собой элемент списка
public class Element<V> {

    private Element<V> next;// <- ссылка на следующий элемент
    private V value; // <- значение в списке

    public Element(V value) {
        this.next = null;
        this.value = value;
    }

    public Element<V> getNext() {
        return next;
    }

    public void setNext(Element<V> next) {
        this.next = next;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

}
