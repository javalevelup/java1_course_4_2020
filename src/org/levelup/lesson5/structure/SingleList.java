package org.levelup.lesson5.structure;

import org.levelup.lesson7.SingleListIterator;

import java.util.Iterator;
import java.util.Objects;

// Однонаправленный связный список
//  - Все элементы списка расположены в памяти хаотично
//  - У каждого элемент есть ссылка на следующий элемент
//  - Элемент списка - он состоит из двух частей
//          - ссылка на следующий элемент
//          - значение
public class SingleList<T> extends AbstractList<T> implements Iterable<T> {

    // Голова списка (начало списка)
    // Элемент списка, с которого он начинается
    private Element head; // <- если head = null, то список пуст

    // Хвост списка (конец списка)

    private class Element {
        private Element next;
        T value;
        Element(T value) {
            this.value = value;
        }
    }

    @Override
    public void add(T value) {
        // Новый элемент в списке
        Element el = new Element(value); // 67cde
        if (head == null) {
            head = el; // 67cde
        } else {
            // Указатель на текущий элемент в поиске
            Element current = head; // дополнительная ссылка, которую можем изменять
            while (current.next != null) {
                current = current.next;
            }
            // current после цикла ссылается на последний элемент
            current.next = el;
        }
        size++;
    }

    @Override
    public void removeByValue(int value) {

    }

    @Override
    public Iterator<T> iterator() {
        return new SingleListIterator(head);
    }

    private class SingleListIterator implements Iterator<T> {

        Element current;

        SingleListIterator(Element current) {
            this.current = current;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            T value = current.value;
            current = current.next;
            return value;
        }
    }

}
