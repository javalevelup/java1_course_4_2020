package org.levelup.lesson5.structure;

// Интерфейс - полностью абстрактный класс
//  - Все методы методы интерфейса абстрактные (public abstract)
//  - У интерфейса нет конструкторов
//  - Все поля интерфейсов представляют собой константы
//          - public static final
//  - Класс может реализовывать бесконечное количество интерфейсов
public interface Structure<T> {

    // int a = Structure.A;
    // public static final int A = 0;
    void add(T value);

    void removeByValue(int value);

    int getSize();

    boolean isEmpty();

}
