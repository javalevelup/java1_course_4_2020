package org.levelup.lesson6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CollectionExample {

    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();

        collection.add("s1");
        collection.add("s2");
        collection.add("s3");
        collection.add("s4");
        collection.add("s5");

        boolean findS4 = collection.contains("s4");
        System.out.println("Нашли s4: " + findS4);
        System.out.println("Размер s4: " + collection.size());

        List<Integer> list = new LinkedList<>();
        list.add(45);
        list.add(65);
        list.add(3245);
        list.add(456);

        list.add(2, 454); // -> 45, 65, 454, 3245, 456
        list.set(3, 323);
        System.out.println(list);

        // Создание копии коллекции
        List<Integer> listCopy = new ArrayList<>(list);

        // хэштаблица
        Map<String, Integer> words = new HashMap<>();
        words.put("the", 434);
        words.put("a", 234);
        words.put("cinema", 56);
        words.put("theatre", 34);

        Integer wordCinemaCount = words.get("cinema");
        System.out.println(wordCinemaCount);

        // foreach - только с классами, которые реализуют интерфейс Iterable<>
        // for (<Тип Generic/Тип массива> название_переменной  : <объект класса, которые реализуют интерфейс Iterable<>>)
        for (String s : collection) {
            // 1 iteration: s = первому элементу коллекции
            // 2 iteration: s = второму элементу коллекции
            // 3 iteration: s = третьему элементу коллекции
            // 4 iteration: s = четвертому элементу коллекции
            System.out.println(s);
        }

        // Ограничения foreach
        //  - Нельзя добавлять или удалять элементы в момент работы foreach
        //  - Если вы это сделаете, то тогда будет выброшена ошибка ConcurrentModificationException (CME)
        for (Integer value: list) {
            System.out.println(value);
//            list.add(4);
//            if (value == 5) {
//                list.remove(value);
//            }
        }

        // Каждый foreach - это цикл while
        Iterator<Integer> iterator = list.iterator();
        // boolean hasNext() - true/false - показывает, есть ли еще элементы
        // T next() - получить следующий элемент и передвинуть курсор
        while (iterator.hasNext()) {
            Integer val = iterator.next();
            System.out.println(val);
            if (val == 5) {
                iterator.remove(); // никаких ошибок не будет :)
            }
        }


    }

}
