package org.levelup.lesson6;

@SuppressWarnings("ALL")
public class PrimitiveAsObject {

    public static void main(String[] args) {

        int intValue = 45;              // primitive
        Integer integerValue = 542;     // object

        char c = '4';
        Character character = 'c';

        double doubleValue = 53.4d;
        Double doubleRefValue = 543.3d;

        // Из примитива в объект класса-обертки - boxing (autoboxing)
        // Из объект класса-обертки в примитива - unboxing (autoboxing)

        // boxing
        // int -> Integer
        Integer i = Integer.valueOf(43); // boxing

        // unboxing
        // Integer -> int
        int primitive = i.intValue(); // unboxing

    }

}
