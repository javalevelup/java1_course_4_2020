package org.levelup.lesson6.generic;

public class GenericApp {

    public static void main(String[] args) {

        GenericClass<String> stringClass = new GenericClass<>(); // <> - diamond operator
        stringClass.setValue("some string");
        // stringClass.setValue(55456); <- compilation error
        // stringClass.setValue(new Object()); <- compilation error

        String value = stringClass.getValue();
        System.out.println(value);

        GenericClass<Integer> integerClass = new GenericClass<>();
        integerClass.setValue(5345);
        // integerClass.setValue("some string"); <- compilation error


    }

}
