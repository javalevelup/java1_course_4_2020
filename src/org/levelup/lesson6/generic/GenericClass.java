package org.levelup.lesson6.generic;

// Generics
// Type erasure - стирание типов
public class GenericClass<TYPE> { // <- TYPE - это тип, который можно использовать внутри класса для обозначения
    // различных сущностей в классе (типов полей, параметров методов и типов возвращаемых значений)

    // private Object value;
    private TYPE value; // TYPE после компиляции превращается в Object

    public void setValue(TYPE value) {
        this.value = value;
    }

    public TYPE getValue() {
        return value;
    }

}
