package org.levelup.lesson7;

import org.levelup.lesson5.structure.SingleList;

import java.util.Iterator;

public class IteratorExample {

    public static void main(String[] args) {
        SingleList<Integer> list = new SingleList<>();
        // SingleList list = new SingleList(); -> SingleList<Object> list = new SingleList<>();
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);

        for (Integer el : list) {
            System.out.println(el);
        }

        System.out.println();
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            Integer el = iterator.next();
            System.out.println(el);
        }

    }

}
