package org.levelup.lesson7;

import org.levelup.lesson5.structure.Element;

import java.util.Iterator;

public class SingleListIterator<T> implements Iterator<T> {

    private Element<T> current; // Текущий элемент в коллекции

    public SingleListIterator(Element<T> head) {
        this.current = head;
    }

    // Сообщает, если ли дальше элемент
    @Override
    public boolean hasNext() {
        // [ 3, 5, 6, 8]
        // current = 6
        // hasNext(): current.getNext() != null -> 6.getNext() -> 8 -> yes
        // next()
        // current = 8
        // hasNext(): current.getNext() != null -> 8.getNext() -> null -> no
        // 8 -> hasNext() : true
        return current != null;
    }

    // Отдает элемент и передвигает указатель к следующему
    @Override
    public T next() {
        // Получаем значение текущего элемента
        T value = current.getValue();
        // Передвигаем указатель
        current = current.getNext();
        return value;
    }

}
