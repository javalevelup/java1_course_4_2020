package org.levelup.lesson7.compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Comparison method violates its general contract!
// compare (compareTo)
//  - если compare(a, b) вернуло 1, то для compare(b, a) должно вернуться -1
public class CompareObjects {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();

        persons.add(new Person("A", 34));
        persons.add(new Person("B", 15));
        persons.add(new Person("C", 46));
        persons.add(new Person("AB", 35));
        persons.add(new Person("A", 55));
        persons.add(new Person("D", 20));

        Collections.sort(persons, new PersonNameComparator());

        for (Person person : persons) {
            System.out.println(person.getName() + " " + person.getAge());
        }

    }

//    static int factorial(int n) {
//        return n == 1 ? 1 : n * factorial(n - 1);
//    }
//    static int factorial(int n) {
//        int fact = 1;
//        for (int i = 1; i < n; i++) {
//            fact *= i;
//        }
//        return fact;
//    }

}
