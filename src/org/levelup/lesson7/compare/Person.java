package org.levelup.lesson7.compare;

// 2 способа сравнивать объекты:
//  - реализовать интерфейс Comparable<T>
//      - Его реализует класс, объекты которого будут учавствовать в сортировке (в сравнении)
//  - реализовать интерфейс Comparator<T>
//      - Пишется отдельный класс, который описывает как сортировать объекты
public class Person implements Comparable<Person> {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    // Сравнение объектов
    //  Person a; Person b;
    //  a.compareTo(b) - по аналогии с equals: a.equals(b)
    // результат работы метод compareTo < 0 (-1) -> a < b
    // результат работы метод compareTo = 0 (0) -> a = b
    // результат работы метод compareTo > 0 (1) -> a > b
    @Override
    public int compareTo(Person o) {
        // Сравнение по возврасту

        // return Integer.compare(age, o.age);
        // return age - o.age;

        if (age < o.age) return -1;
        if (age == o.age) return 0;
        return 1;
    }

}
