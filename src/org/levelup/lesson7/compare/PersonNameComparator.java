package org.levelup.lesson7.compare;

import java.util.Comparator;

public class PersonNameComparator implements Comparator<Person> {

    // Сравнение объектов
    // PersonNameComparator comparator; Person a; Person b;
    // int result = comparator.compare(a, b);
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
