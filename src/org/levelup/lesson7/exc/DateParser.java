package org.levelup.lesson7.exc;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

    // Преобразование строки в дату
    public Date parseDate(String dateAsString) {
        // Date - long число - количество милисекунд, которые проешли с 1 января 1970
        // 0 - 1 января 1970 00:00:00.000
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        // try-catch-finally

        try {
            System.out.println("Пытаемся преобразовать строку в дату");
            Date date = formatter.parse(dateAsString);
            System.out.println("Преобразование прошло успешно");
            return date;
        } catch (ParseException exc) {
            // exc.printStackTrace();
            System.out.println("Неправильный формат даты");
            return null;
        }
    }

}
