package org.levelup.lesson7.exc;

public class Stack {

    private int[] elements = new int[5];
    private int size;

    // положить на стек
    public void push(int value) throws StackOverflowException {
        if (size == elements.length) {
            throw new StackOverflowException();
        }
        elements[size++] = value;
    }

    // получить вершину стека
    public int pop() {
        if (size == 0) {
            EmptyStackException exc = new EmptyStackException();
            throw exc;
        }
        // code ... here
        return elements[--size];
    }

}
