package org.levelup.lesson7.exc;

// checked exception (проверяемое исключение)
public class StackOverflowException extends Exception {

    public StackOverflowException() {
        super("Stack overflow");
    }

}
