package org.levelup.lesson8;

public class Car {

    static int wheelCount = 4;
    String brand;

    public Car(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public static int changeAndGetWheelCount(int count) {
        if (count >= 2) {
            wheelCount = count;
            return wheelCount;
        }
        return wheelCount;
    }

    public static Car createCar(String brand) {
        if (brand != null && !brand.isEmpty()) {
            Car car = new Car(brand);
            car.brand = brand;
            return car;
        }
        return null;
    }

}
