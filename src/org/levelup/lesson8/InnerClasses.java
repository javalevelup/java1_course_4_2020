package org.levelup.lesson8;

public class InnerClasses {

    public static void main(String[] args) {

        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();

        innerClass.setPrivateInts(15, 53);

        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();

        Person.PersonBuilder builder = new Person.PersonBuilder();
        Person person = builder
                .name("Dmitry")
                .lastName("Protsko")
                .age(20)
                .build();
    }

}
