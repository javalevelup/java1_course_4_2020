package org.levelup.lesson8;

// Внешний класс
public class OuterClass {

    private static int staticPrivateInt;
    private int privateInt;

    // Внутренние классы
    //  - Внутренний класс (Inner class)
    //  - Вложенный класс (Nested class)

    // Внутренние классы (Inner и Nested) могут иметь любой модификатор доступа
    public class InnerClass {

        public void setPrivateInts(int f, int s) {
            privateInt = f;
            staticPrivateInt = s;
        }

    }

    public static class NestedClass {

        public void setPrimitive(int s) {
            staticPrivateInt = s;
            // privateInt = 4;
        }

    }

}
