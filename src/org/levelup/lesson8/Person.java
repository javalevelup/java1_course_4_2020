package org.levelup.lesson8;

public class Person {

    // antipattern - TCP - telescoping constructor parameters
    // Вы не должны создавать конструкторы с 5 и более аргументами
    // pattern - Builder

    private String name;
    private String lastName;
    private int age;

    // Запретили создавать объекты класса Person извне
    private Person() {}

    public static class PersonBuilder {

        private Person person = new Person();

        // withName(String name)

        // builder.name("").lastName("").age(4);

        // builder.name("");
        // builder.lastName("");
        // builder.age(45);

        public PersonBuilder name(String name) {
            person.name = name;
            return this;
        }

        public PersonBuilder lastName(String lastName) {
            person.lastName = lastName;
            return this;
        }

        public PersonBuilder age(int age) {
            person.age = age;
            return this;
        }

        public Person build() {
            if (person.age <= 0) {
                throw new RuntimeException("Возраст человека не может быть меньше или равен 0");
            }
            return person;
        }

    }

}
