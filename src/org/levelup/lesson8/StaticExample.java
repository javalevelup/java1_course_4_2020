package org.levelup.lesson8;

public class StaticExample {

    public static void main(String[] args) {

        Car car = new Car("Lada");
        System.out.println(car.getBrand());

        System.out.println(Car.wheelCount);
        int wheelCount = Car.changeAndGetWheelCount(6);
        System.out.println(wheelCount);
    }

}
