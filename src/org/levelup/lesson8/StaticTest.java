package org.levelup.lesson8;

@SuppressWarnings("ALL")
public class StaticTest {

    private static void print() {
        System.out.println("Hello world!");
    }

    public static void main(String[] args) {
        StaticTest test = null;
        test.print(); // -> StaticTest.print();
    }

}
