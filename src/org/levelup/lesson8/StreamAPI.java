package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("ALL")
public class StreamAPI {

    public static void main(String[] args) {

        // InnerClasses
        // java compiler: OuterClass.java -> OuterClass.class
        // OuterClass.InnerClass -> OuterClass$InnerClass.class

        // Anonymous Inner Class - Аннонимный внутренний класс
        List<String> words = new ArrayList<>();
        words.add("a");
        words.add("milk");
        words.add("dock");
        words.add("Desk");
        words.add("island");
        words.add("cucumber");

        // Анонимный внутренний класс - имеет имя "1"
        // StreamAPI.class, StreamAPI$1.class <- comparator
        Comparator<String> comparator = new Comparator<>() {
            @Override
            public int compare(String o1, String o2) {
                // o1.toLowerCase().compareTo(o2.toLowerCase())
                return o1.compareToIgnoreCase(o2);
            }
        };

        // AIC: StreamAPI$2.class
        Car car = new Car("LADA") {
            @Override
            public String getBrand() {
                return brand + " - aic";
            }
        };
        //System.out.println(car.getClass().getName());
        //System.out.println(car.getClass().getSuperclass().getName());
        //System.out.println(comparator.getClass().getSuperclass().getName());
        // System.out.println(Arrays.stream(comparator.getClass().getInterfaces()).map(c -> c.getName()).collect(Collectors.joining(",")));
        //System.out.println(car.getBrand());

        // Сортировка по алфавиту (если не использовать собсвенный компаратор)
        Collections.sort(words, comparator);
        for (String word : words) {
            System.out.println(word);
        }

        int maxLength = 5;
        // boolean test(T value)
        Predicate<String> lengthPredicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() >= maxLength;
            }
        };

        Collection<String> filteredWords = new ArrayList<>();
        for (String word : words) {
            if (lengthPredicate.test(word)) {
                filteredWords.add(word);
            }
        }

        System.out.println();
        for (String word : filteredWords) {
            System.out.println(word);
        }

        // Lambda
        //  ->
        // (method arguments) -> { ... method body ... }

        // arguments
        // interface FInterface { void method(); }
        // () <- метод не имеет аргументов

        // interface FInterface { void method(int a); }
        // (a) или (value) или val <- метод имеет один аргумент (скобки () можно не писать)

        // interface FInterface { void method(int a, double b); }
        // (a, b) или (val1, val2) <- метод имеет 2 и более аргументов

        // method body
        // interface FInterface { void method(int a); }
        // Когда в методе только одно выражение
        // a -> sout(a) <- без скобок {}, без return и без ;

        // interface FInterface { int increase(int a); }
        // a -> a + 10 <- если действие возвращает тот же тип, что и тип возвращяемого значения, то можно не писать return

        // interface FInterface { int method(int a, int b); }
        // (a, b) -> { a = a + 10; b = b + 20; return a + b; } <- когда несколько выражений в методе

        //  Predicate<String> lengthPredicate = new Predicate<String>() {
        //            @Override
        //            public boolean test(String s) {
        //                return s.length() >= maxLength;
        //            }
        //        };
        Predicate<String> lambdaPredicate = string -> string.length() >= maxLength;
        // Comparator<String> comparator = new Comparator<>() {
        //            @Override
        //            public int compare(String o1, String o2) {
        //                // o1.toLowerCase().compareTo(o2.toLowerCase())
        //                return o1.compareToIgnoreCase(o2);
        //            }
        //        };
        Comparator<String> lambdaComparator = (s1, s2) -> s1.compareToIgnoreCase(s2);

        // Java8 -> StreamAPI
        // Набор кода для облегчения работы с коллекциями и массивами

        // Фильтрация коллекции слов
        List<String> longWords = words.stream() // -> возвращает объект Stream
                .filter(word -> word.length() >= maxLength) // если true, то тогда элемент остается в Stream, иначе удаляется
                .collect(Collectors.toList());
        // Вывести коллекцию на консоль
        System.out.println();
        longWords.forEach(longWord -> System.out.println(longWord));

        // Изменить данные в коллекции и вывести на консоль
        System.out.println();
        words.stream()
                // .map(word -> return word.toUpperCase())
                .map(word -> word.toUpperCase()) // <- используется для преобразования (изменения) каждого элемента коллекции
                .forEach(word -> System.out.println(word));

        Collection<Integer> integers = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < 20; i++) {
            integers.add(r.nextInt(20));
        }

        System.out.println();

        integers.forEach(i -> System.out.print(i + " "));

        System.out.println();
        System.out.println();

        List<Integer> firstTenElements = integers.stream()
                // .skip() <- сколько пропустить элементов сначала
                .limit(10)
                .collect(Collectors.toList());
        firstTenElements.forEach(i -> System.out.print(i + " "));

        System.out.println();
        System.out.println();

        // Set<Integer> uniqueInteger = new HashSet<>(integers);
        // Найдите сумму уникальных чисел
        int sum = integers.stream()
                .distinct() // <- удаляет дубликаты
                // Stream<Integer> -> IntStream
                // ToIntFunction#int applyToInt(T value) -> { return value; }
                .mapToInt(value -> value)
                .sum();

        // Сгенерируйте коллекцию целых чисел (любой тип коллекции, но для простоты можете использовать ArrayList), используя класс Random(). Используя методы классов-коллекций:
        //
        //1. Создайте новую коллекцию, переписав в него часть элементов из первой коллекции (к примеру, первые 10 элементов)
        //2. Найдите все уникальные числа (те, которые встречаются один раз) и сохраните их в отдельную коллекцию (
        //3. Отсортируйте коллекцию (по возрастанию/убыванию)
        //4. Найдите максимум, минимум и сумму чисел коллекции
        //5. Создайте  коллекцию, содержащий все положительные числа первой коллекции
        //6. Удалите из первого коллекции все нечетные числа
        //7. Найдите индекс указанного числа в коллекции

    }

}
