package org.levelup.lesson9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class FReader {

    // Метод для чтения файла
    public void readFile(String filepath) {
        // Input/Output streams

        // Для работы с входными данными
        // InputStream
        // Reader

        // Для работы с выходными данными
        // OutputStream
        // Writer

        // FileInputStream
        FileInputStream fis = null; // <- внешний ресурс
        try {
            fis = new FileInputStream(new File(filepath)); // <- throws FileNotFoundException

            byte[] buffer = new byte[256]; // <- в этот вы считываете данные
            int count;
            do {
                count = fis.read(buffer); // <- метод read - возвращает количетсво байт, которые были считаны
                for (int i = 0; i < count; i++) {
                    System.out.print(buffer[i] + ", ");
                }
                System.out.println();

                if (count != -1) {
                    String text = new String(buffer, 0, count);
                    System.out.println(text);
                }

            } while (count != -1);

//
//            while ( (fis.read(buffer)) != -1 ) { // <- -1 - это маркер того, что файл закончилася
//                System.out.println("Byte array: " + Arrays.toString(buffer));
//                System.out.println("String: " + new String(buffer));
//            }

        } catch (FileNotFoundException exc) {
            System.out.println("Невозможно найти файл по заданному пути: " + filepath);
        } catch (IOException exc) {
            exc.printStackTrace(); // <- печать трассировки стека вызовов функций
            System.out.println("Произошла ошибка чтения файла " + filepath);
        } finally {
            // Код в этом блоке выполняется всегда
            // Нужно закрыть соединение
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException exc) {
                     // переброс исключений (оборачивание одного exception в другой)
                    throw new RuntimeException(exc);
                }
            }
        }
    }

    public void writeFile(String filepath) {
        // try-with-resources
        // FileWriter
        String text = "Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. " +
                "Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. " +
                "Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в " +
                "Lorem Ipsum, \"consectetur\", и занялся его поисками в классической латинской литературе. В результате он нашёл " +
                "неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги \"de Finibus Bonorum et Malorum\" " +
                "(\"О пределах добра и зла\"), написанной Цицероном в 45 году н.э. Этот трактат по теории этики был очень " +
                "популярен в эпоху Возрождения. Первая строка Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", происходит от " +
                "одной из строк в разделе 1.10.32";

        // try (...resources...)
        try (FileWriter fw = new FileWriter(new File(filepath), true)) {
            fw.write(text);
            fw.write("\n");
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }

    }

}
