package org.levelup.lesson9;

import java.io.File;
import java.io.IOException;

public class FileExamples {

    public static void main(String[] args) throws IOException {
        // /Users/dmitrijprocko/IdeaProjects/java_1_course_4_2020/src/org/levelup/lesson9/FileExamples.java

        // src: /org/levelup/lesson9/FileExamples.java
        // java_1_course_4_2020:  /src/org/levelup/lesson9/FileExamples.java
        // lesson9: FileExamples.java

        //
        File txtFile = new File("text.txt"); // <- относительный путь (относительно java_1_course_4_2020)
        // Проверка на то, что файл сущетсвует в реальности
        boolean isExist = txtFile.exists();
        System.out.println("Is exist: " + isExist);

        // Создание файла
        boolean created = txtFile.createNewFile(); // true, если файл создался, false в любых других случаях
        System.out.println("File created: " + created);

        boolean deleted = txtFile.delete();
        System.out.println("File deleted: " + deleted);

        // File представляет собой и файл, и папку
        File srcDir = new File("src/");
        System.out.println("Is directory: " + srcDir.isDirectory());
        System.out.println("Is file: " + srcDir.isFile());

    }

}
