package org.levelup.lesson9;

public class InitializationBlocks {

    static {
        System.out.println("Static 1");
    }

    {
        System.out.println("Usual 1");
    }

    {
        System.out.println("Usual 2");
    }

    public InitializationBlocks() {
        System.out.println("Constructor");
    }

    {
        System.out.println("Usual 3");
    }

    static {
        System.out.println("Static 2");
    }

    public static void main(String[] args) {
        new InitializationBlocks();
        new InitializationBlocks();
    }

}
